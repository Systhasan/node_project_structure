/**
 * @type include
 * @name express, body-parser, faker, lodash for times, lodash for random, 
 * @desc include node module for our node application.
 */
const express = require("express");
const bodyParser = require("body-parser");
const seeders = require("./db/seeds/seeders");
const routes = require("./routes/routes");

// console.log(seeders.favoriteBook());
/** 
 * @type include
 * @desc load all api routes.
*/
const api_routes = require('./api');


/**
 * @type    init express
 * @params  
 * @desc    init express and hold that object to the variable named app.
 */
const app = express();


/**
 * @type middleware
 * @desc 
 */
app.use(bodyParser.json());
app.use(express.static("app/public"));


/** 
 * @type init
 * @params {obj} app
 * @desc Load our api routes.
*/
api_routes(app);
routes(app, express);

app.get('/', (req,res) => {
    res.send('node server is running.');
});

/** 
 * @type route
 * @params 
 * @desc route for add demo data to database.
*/
app.get('/demo-data/add', (req, res)=> {
    seeders.addSeeder();
    res.send('Demo data added successfully');
});


/** 
 * @type    init server
 * @params  {int} port number, 
 *          {function} callback 
 * @desc    start our server to the given port number.
*/
app.listen(8080, () => console.log('server start on 8080 port number.'));