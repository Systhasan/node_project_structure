// include all api routes
const article = require('./Article/Article_api');
const author = require('./Author/Author_api');

/**
 * @type 
 * @params  {obj} app
 * @desc    add our all api routes to an object.
 */
module.exports = (app) => {
    // Article API
    app.get('/api/article', article.getAllArticle);
    
    // Author API
    app.get('/api/author', author.getAllAuthor);
    app.get('/api/author/:id', author.getAuthorById);
};