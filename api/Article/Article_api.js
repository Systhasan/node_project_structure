/** 
 * @type include
 * @name db
 * @desc include models object
*/
const db = require('../../db/models');

module.exports = {
    
    getAllArticle(req, res){
        db.article.findAll({
            include: [{
                model: db.author
            }]
        })
        .then( (result) => {res.json(result)})
        .catch( (err) => {res.json(err)});
    }
};