/** 
 * @type include
 * @name db
 * @desc include models object
*/
const db = require('../../db/models');

module.exports = {

    getAllAuthor(req, res){
        db.author.findAll()
        .then((result)=>res.json(result))
        .catch((err)=>res.json(err));
    },

    getAuthorById(req, res){
        res.send(req.params.id);
    }

};