const faker = require("faker");
const times = require("lodash.times");
const random = require("lodash.random");
const db = require('../../db/models');


/** 
 * @type method
 * @params 
 * @return null
 * @desc using this method we can add demo data to the database.
*/
function insertDataToDataBase() {
    db.sequelize.sync().then(() => {
        db.article.bulkCreate(
            times(10, () => ({
                firstName : faker.name.findName(),
                lastName: faker.name.findName()
            }))
        );

        db.author.bulkCreate(
            times(10, () => ({
                firstName: faker.name.findName(),
                lastName: faker.name.findName(),
                email : faker.internet.email(),
                address: faker.address.streetAddress()+' '+faker.address.streetName(),
                teacherId: random(1,10)
            }))
        );
    }).catch((err)=>console.log(err));
    return;
}




module.exports.addSeeder = insertDataToDataBase;