module.exports = (sequelize, DataTypes ) => {
    const Author = sequelize.define('author', {
        id : {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        firstName:{
            type: DataTypes.STRING,
            allowNull: false
        },
        lastName:{
            type: DataTypes.STRING,
            allowNull: false
        },
        email:{
            type:DataTypes.STRING,
            allowNull:false
        },
        address:{
            type:DataTypes.TEXT,
            allowNull:false
        }
    },
        {
            freezeTableName: true
        }
    );

    Author.associate = (models) => {
        Author.belongsTo(models.article);
    };
    
    return Author;
};
