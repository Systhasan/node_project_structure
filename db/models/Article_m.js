module.exports = (sequelize, DataTypes ) => {
    const Article = sequelize.define('article', {
        id : {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        firstName:{
            type: DataTypes.STRING,
            allowNull: false
        },
        lastName:{
            type: DataTypes.STRING,
            allowNull: false
        },
    },
        {
            freezeTableName: true
        }
    );

    Article.associate = (models) => {
        Article.hasMany(models.author);
    };
    
    return Article;
};
