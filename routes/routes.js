const home = require('./controllers/Home_C');

module.exports = (app, express) => {

    /**
     * @type    init express router
     * @params  
     * @desc    init express router.
     */
    const router  = express.Router();

    /**
     * @type    Middleware
     * @params  
     * @desc    express middleware. 
     */
    app.use('/', router);
    
    router.get('/home/test', home.test);

    router.get('/blog/test', (req, res) => {
        res.send('Blog Router');
    });

};